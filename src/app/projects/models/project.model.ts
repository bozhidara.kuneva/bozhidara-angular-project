export class Project {
  id: number;
  name: string;
  estimate: number;
}
